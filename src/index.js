import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

const client = new ApolloClient({
  uri: "https://api.8base.com/ck1ulwlsc000201mc2p335yml"
});

ReactDOM.render(
  <ApolloProvider client={client}>
   <Router>
    <Switch>
      <Route exact path="/" component={App} />
     </Switch>
    </Router>
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
