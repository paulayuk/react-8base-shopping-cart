import React from 'react';
//import {CartContext} from './CartContext';

const CartItem = (props) => {

  return (
      <div class="col-sm-4">
          <div class="card" style={{width: "18rem"}}>
            <img src={props.item.image.downloadUrl} class="card-img-top" alt="shirt"/>
            <div class="card-body">
              <h5 class="card-title">{props.item.name}</h5>
              <h6 class="card-title">$ {props.item.price}</h6>
              <button class="btn btn-primary">Buy now</button>
            </div>
          </div>
      </div>
  );

}

export default CartItem;
